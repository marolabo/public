﻿/*
 * Display.cs
 * - PrintMonitorのテストのためのGUIツール。
 * - だいたいの使い方
 *   1．「ジョブ待機開始」ボタン押下でPrintMonitorによる印刷ジョブ監視を開始する。
 *   2．何かを印刷する。
 *   3．「ログの場所を開く」ボタン押下でログディレクトリを開いてみるとコールバック関数によりログファイルが生成されているのが確認できる。
 *   4．「結果を表示する」ボタン押下で画面に印刷情報が表示される。印刷情報が取得できていなければ何も表示されない。
 *   5．「ログの場所を開く」ボタン押下でログディレクトリを開いてみると「結果を表示する」ボタン押下によりログファイルが生成されているのが確認できる。
 *   6．「結果を表示する」ボタン押下で画面がクリアされる。でもここでこのボタンを押さなくても特に問題ない。
 *   7．「ジョブ待機終了」ボタン押下で処理を終了する。でもこのボタンを押さずに画面を閉じてもいい。
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Management;
using System.Diagnostics;

namespace PrintMonitor
{
    public partial class Display : Form
    {
        public Display()
        {
            InitializeComponent();
        }

        static PrintMonitor printJobGetter = new PrintMonitor();
        static PrintInfo info = null;
        static String logName = "";

        private void Display_Load(object sender, EventArgs e)
        {
            //test();

            init();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            printJobGetter.debugMode = true;
            printJobGetter.PrintDetectedEvent += new PrintMonitor.PrintMonitorEventHandler(PrintDetectedCallBack);    //イベントで情報取得完了通知
        }

        //表示内容を初期化する関数
        private void init()
        {
            lb_waitStatus.Text = "ジョブを監視していません。";
            lb_printStatus.Text = "";
            lb_date.Text = "－";
            lb_size.Text = "－";
            lb_window.Text = "－";
            lb_page.Text = "－";
            lb_printer.Text = "－";
            lb_color.Text = "－";
            lb_duplex.Text = "－";
            lb_exePath.Text = "－";
        }

        //Start
        private void btn_start_Click(object sender, EventArgs e)
        {
            lb_waitStatus.Text = "印刷ジョブを監視中...";
            lb_printStatus.Text = "印刷が終わったら[結果を表示する]ボタンを押下してください。";

            info = new PrintInfo();
            printJobGetter.Detected = GetPrintResult;
            printJobGetter.Start();
        }

        //ジョブから取得した情報を処理するAction用関数
        private static void GetPrintResult(PrintInfo getter)
        {
            info = new PrintInfo();
            String msg = "";
            bool logResult = false;
            try
            {
                info.AccessTime = getter.AccessTime;
                info.Size = getter.Size;
                info.Title = getter.Title;
                info.PageNum = getter.PageNum;
                info.ExePath = getter.ExePath;
                info.Duplex = getter.Duplex;
                info.Color = getter.Color;
                info.PrinterName = getter.PrinterName;

                logName = "Print_" + info.Size.Replace(" x ", "x") + "_p" + info.PageNum + "_col-" + info.Color + "_dup-" + info.Duplex + "_" + info.PrinterName;
                msg = "AccessTime  -> " + info.AccessTime + "\r\n";
                msg += "Size        -> " + info.Size + "\r\n";
                msg += "Title       -> " + info.Title + "\r\n";
                msg += "PageNum     -> " + info.PageNum + "\r\n";
                msg += "PrinterName -> " + info.PrinterName + "\r\n";
                msg += "Duplex      -> " + info.Duplex + "\r\n";
                msg += "Color       -> " + info.Color + "\r\n";
                msg += "ExePath     -> " + info.ExePath + "\r\n";
                logResult = Util.logging(logName, msg);
            }
            catch (Exception ex)
            {
                logName = "ErrorLog";
                logResult = Util.logging(logName, "Tester error -> " + ex.Message);
                Console.WriteLine(ex.Message);
            }
        }

        //印刷情報取得完了時のコールバック
        private void PrintDetectedCallBack(PrintMonitorEventArgs e)
        {
            //一応イベントでの情報取得完了通知の検証のためデバッグメッセージをファイルに出力してみる
            //取得した印刷情報は画面表示時にクリアする
            if (e.EventType == PrintMonitorEventArgs.TYPE_PRINT_DETECTED)
            {
                if (printJobGetter.debugMode && printJobGetter.debugMessage != "")
                {
                    Util.logging("DebugCallbackLog_PrintOperationManagerTest_" + DateTime.Now.ToString("yyyyMMddHHmmss"), printJobGetter.debugMessage);
                }
            }
        }

        //Stop
        private void btn_end_Click(object sender, EventArgs e)
        {
            printJobGetter.Stop();
            lb_waitStatus.Text = "ジョブ監視を停止しました。";
            lb_printStatus.Text = "";
            info = null;
        }

        //結果を表示する
        private void btn_output_Click(object sender, EventArgs e)
        {
            PrintInfoOutput();
        }

        void PrintInfoOutput()
        {
            if (info != null && info.AccessTime != new DateTime(0))
            {
                lb_date.Text = info.AccessTime.ToString();
                lb_size.Text = info.Size;
                lb_window.Text = info.Title;
                lb_page.Text = info.PageNum.ToString();
                lb_printer.Text = info.PrinterName;
                if (info.Color)
                {
                    lb_color.Text = "カラー";
                }
                else
                {
                    lb_color.Text = "モノクロ";
                }
                if (info.Duplex)
                {
                    lb_duplex.Text = "両面印刷";
                }
                else
                {
                    lb_duplex.Text = "片面印刷";
                }
                lb_exePath.Text = info.ExePath;

                info = null;
            }
            else
            {
                lb_date.Text = "－";
                lb_size.Text = "－";
                lb_window.Text = "－";
                lb_page.Text = "－";
                lb_printer.Text = "－";
                lb_color.Text = "－";
                lb_duplex.Text = "－";
                lb_exePath.Text = "－";
            }

            PrintInfoLogOutput();
        }

        void PrintInfoLogOutput()
        {
            if (printJobGetter.debugMode && printJobGetter.debugMessage != "")
            {
                Util.logging("DebugLog_PrintOperationManagerTest_" + DateTime.Now.ToString("yyyyMMddHHmmss"), printJobGetter.debugMessage);
                printJobGetter.debugMessage = "";
            }
        }

        //ログの場所を開く
        private void openLogPath_Click(object sender, EventArgs e)
        {
            Process.Start(Util.logDir);
        }

        //Edge判定テスト
        private void test()
        {
            ManagementObjectSearcher procSearcher = new ManagementObjectSearcher("SELECT ProcessId,ExecutablePath FROM Win32_Process Where ProcessId=" + 9420); //注意: EdgeのPIDをHC

            Process[] procList = Process.GetProcesses();
            String pName = "";
            foreach (var proc in procList)
            {
                if (String.IsNullOrWhiteSpace(proc.MainWindowTitle)) continue;
                String query = "SELECT ProcessId,ExecutablePath FROM Win32_Process Where ProcessId=" + proc.Id + "And ExecutablePath Is Not Null";
                procSearcher = new ManagementObjectSearcher(query);
                foreach (ManagementObject item in procSearcher.Get())
                {
                    if (item.GetPropertyValue("ExecutablePath") != null)
                    {
                        pName = item.GetPropertyValue("ExecutablePath").ToString();
                        Console.Write(proc.Id + " : " + proc.MainWindowTitle + " , " + pName);
                        //Edge判定
                        if (proc.MainWindowTitle.ToLower().Contains("microsoft edge") && System.IO.Path.GetFileName(pName).ToLower().Equals("applicationframehost.exe"))
                        {
                            Console.Write(" THIS IS EDGE!!!!");
                        }
                        Console.WriteLine();
                    }
                    break;
                }

            }
            return;
        }

    }
}
