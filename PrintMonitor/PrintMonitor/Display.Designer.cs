﻿namespace PrintMonitor
{
    partial class Display
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_printStatus = new System.Windows.Forms.Label();
            this.btn_end = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.lb_waitStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.openLogPath = new System.Windows.Forms.Button();
            this.btn_output = new System.Windows.Forms.Button();
            this.lb_size = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lb_window = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lb_page = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lb_printer = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lb_color = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lb_duplex = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_exePath = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lb_date = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb_printStatus
            // 
            this.lb_printStatus.AutoSize = true;
            this.lb_printStatus.Location = new System.Drawing.Point(164, 46);
            this.lb_printStatus.Name = "lb_printStatus";
            this.lb_printStatus.Size = new System.Drawing.Size(53, 12);
            this.lb_printStatus.TabIndex = 25;
            this.lb_printStatus.Text = "印刷状態";
            // 
            // btn_end
            // 
            this.btn_end.Location = new System.Drawing.Point(471, 12);
            this.btn_end.Name = "btn_end";
            this.btn_end.Size = new System.Drawing.Size(129, 57);
            this.btn_end.TabIndex = 23;
            this.btn_end.Text = "ジョブ待機終了";
            this.btn_end.UseVisualStyleBackColor = true;
            this.btn_end.Click += new System.EventHandler(this.btn_end_Click);
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(12, 12);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(129, 57);
            this.btn_start.TabIndex = 22;
            this.btn_start.Text = "ジョブ待機開始";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lb_waitStatus
            // 
            this.lb_waitStatus.AutoSize = true;
            this.lb_waitStatus.Location = new System.Drawing.Point(164, 22);
            this.lb_waitStatus.Name = "lb_waitStatus";
            this.lb_waitStatus.Size = new System.Drawing.Size(77, 12);
            this.lb_waitStatus.TabIndex = 24;
            this.lb_waitStatus.Text = "関数実行状態";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.openLogPath);
            this.groupBox1.Controls.Add(this.btn_output);
            this.groupBox1.Controls.Add(this.lb_size);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.lb_window);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.lb_page);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.lb_printer);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lb_color);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lb_duplex);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lb_exePath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lb_date);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(588, 204);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "印刷結果";
            // 
            // openLogPath
            // 
            this.openLogPath.Location = new System.Drawing.Point(452, 18);
            this.openLogPath.Name = "openLogPath";
            this.openLogPath.Size = new System.Drawing.Size(102, 23);
            this.openLogPath.TabIndex = 17;
            this.openLogPath.Text = "ログの場所を開く";
            this.openLogPath.UseVisualStyleBackColor = true;
            this.openLogPath.Click += new System.EventHandler(this.openLogPath_Click);
            // 
            // btn_output
            // 
            this.btn_output.Location = new System.Drawing.Point(341, 18);
            this.btn_output.Name = "btn_output";
            this.btn_output.Size = new System.Drawing.Size(89, 23);
            this.btn_output.TabIndex = 16;
            this.btn_output.Text = "結果を表示する";
            this.btn_output.UseVisualStyleBackColor = true;
            this.btn_output.Click += new System.EventHandler(this.btn_output_Click);
            // 
            // lb_size
            // 
            this.lb_size.AutoSize = true;
            this.lb_size.Location = new System.Drawing.Point(107, 44);
            this.lb_size.Name = "lb_size";
            this.lb_size.Size = new System.Drawing.Size(58, 12);
            this.lb_size.TabIndex = 15;
            this.lb_size.Text = "用紙サイズ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(37, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 12);
            this.label16.TabIndex = 14;
            this.label16.Text = "用紙サイズ：";
            // 
            // lb_window
            // 
            this.lb_window.AutoSize = true;
            this.lb_window.Location = new System.Drawing.Point(107, 68);
            this.lb_window.Name = "lb_window";
            this.lb_window.Size = new System.Drawing.Size(41, 12);
            this.lb_window.TabIndex = 13;
            this.lb_window.Text = "印刷物";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(54, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 12);
            this.label14.TabIndex = 12;
            this.label14.Text = "印刷物：";
            // 
            // lb_page
            // 
            this.lb_page.AutoSize = true;
            this.lb_page.Location = new System.Drawing.Point(107, 91);
            this.lb_page.Name = "lb_page";
            this.lb_page.Size = new System.Drawing.Size(71, 12);
            this.lb_page.TabIndex = 11;
            this.lb_page.Text = "印刷ページ数";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 12);
            this.label12.TabIndex = 10;
            this.label12.Text = "印刷ページ数：";
            // 
            // lb_printer
            // 
            this.lb_printer.AutoSize = true;
            this.lb_printer.Location = new System.Drawing.Point(107, 113);
            this.lb_printer.Name = "lb_printer";
            this.lb_printer.Size = new System.Drawing.Size(38, 12);
            this.lb_printer.TabIndex = 9;
            this.lb_printer.Text = "プリンタ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(57, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 12);
            this.label10.TabIndex = 8;
            this.label10.Text = "プリンタ：";
            // 
            // lb_color
            // 
            this.lb_color.AutoSize = true;
            this.lb_color.Location = new System.Drawing.Point(107, 136);
            this.lb_color.Name = "lb_color";
            this.lb_color.Size = new System.Drawing.Size(17, 12);
            this.lb_color.TabIndex = 7;
            this.lb_color.Text = "色";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(78, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "色：";
            // 
            // lb_duplex
            // 
            this.lb_duplex.AutoSize = true;
            this.lb_duplex.Location = new System.Drawing.Point(107, 158);
            this.lb_duplex.Name = "lb_duplex";
            this.lb_duplex.Size = new System.Drawing.Size(41, 12);
            this.lb_duplex.TabIndex = 5;
            this.lb_duplex.Text = "印刷面";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "印刷面：";
            // 
            // lb_exePath
            // 
            this.lb_exePath.AutoSize = true;
            this.lb_exePath.Location = new System.Drawing.Point(107, 182);
            this.lb_exePath.Name = "lb_exePath";
            this.lb_exePath.Size = new System.Drawing.Size(63, 12);
            this.lb_exePath.TabIndex = 3;
            this.lb_exePath.Text = "実行ファイル";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "実行ファイル：";
            // 
            // lb_date
            // 
            this.lb_date.AutoSize = true;
            this.lb_date.Location = new System.Drawing.Point(107, 23);
            this.lb_date.Name = "lb_date";
            this.lb_date.Size = new System.Drawing.Size(53, 12);
            this.lb_date.TabIndex = 1;
            this.lb_date.Text = "印刷日時";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "印刷日時：";
            // 
            // Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 305);
            this.Controls.Add(this.lb_printStatus);
            this.Controls.Add(this.btn_end);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.lb_waitStatus);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Display";
            this.Text = "PrintMonitor";
            this.Load += new System.EventHandler(this.Display_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_printStatus;
        private System.Windows.Forms.Button btn_end;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label lb_waitStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button openLogPath;
        private System.Windows.Forms.Button btn_output;
        private System.Windows.Forms.Label lb_size;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lb_window;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lb_page;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lb_printer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lb_color;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lb_duplex;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lb_exePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lb_date;
        private System.Windows.Forms.Label label1;
    }
}

