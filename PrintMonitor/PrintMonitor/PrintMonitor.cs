﻿/*
 * PrintMonitor.cs
 * - 印刷ジョブを別スレッドで監視し、印刷物の情報を取得する。
 * - インスタンスを生成してStart()で監視開始、Stop()で監視終了。
 * - 呼び元への印刷情報通知にはActionとEventの2通りの方法を実装している。
 * - PSドライバは完全に対応できるはず。PCLは色情報が必ず「カラー」になる。
 * - 印刷用紙枚数もドライバの種類によってはうまく取得できないかもしれない。
 *   印刷ページ枚数なら印刷ジョブから取得可能なのでコード修正でそう変更してもいいかもしれない(用紙枚数解決関数NumOfPageOptimisation()の実行箇所をコメントアウトするだけでいいとおもう)。
 * - デバッグメッセージには、各印刷ジョブのSTATUSと、その時点で取得できている印刷情報を含む。MSEdgeでの印刷を検知したりしたらそういうメッセージも出力される。
 *   DevModeを含んでいたらその情報も含む。
 * - System.Management.dllとSystem.Printing.dllとReachFramework.dllの参照が必須。
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Drawing.Printing;
using System.Diagnostics;
using System.Printing;
using System.Management;
using System.IO;
using System.Globalization;

namespace PrintMonitor
{
    public class PrintInfo
    {
        public DateTime AccessTime { set; get; }    //印刷日時
        public string Size { set; get; }            //用紙サイズ
        public string Title { set; get; }           //印刷物名
        public int PageNum { set; get; }            //印刷用紙枚数(実際に出力された紙の枚数)  一部ドライバでは複数ページ割り付けとかに対応できないかもしれない
        public string PrinterName { set; get; }     //印刷に用いたプリンタの名称
        public bool Color { set; get; }             //印刷色(カラーorモノクロ)    これも一部ドライバは白黒印刷でも「カラー」と言い張る
        public bool Duplex { set; get; }            //両面印刷か否か
        public string ExePath { set; get; }         //印刷に用いた実行ファイルのファイルパス   右クリックで印刷されたりすると空になる

        public void Copy(PrintInfo src)
        {
            this.AccessTime = src.AccessTime;
            this.Size = src.Size;
            this.Title = src.Title;
            this.PageNum = src.PageNum;
            this.PrinterName = src.PrinterName;
            this.Color = src.Color;
            this.Duplex = src.Duplex;
            this.ExePath = src.ExePath;
        }
    }

    public interface IManager
    {
        void Start();
        void Stop();
    }

    public class PrintMonitor : IManager
	{
        /***** Import *****/
        [DllImport("winspool.drv", SetLastError = true)]
            public static extern int DeviceCapabilities(string lpDeviceName, string lpPort, int iIndex, IntPtr lpOutput, IntPtr lpDevMode);
        [DllImport("winspool.drv", EntryPoint = "OpenPrinter", CallingConvention = CallingConvention.StdCall)]
            public static extern bool OpenPrinter(String pPrinterName, out IntPtr phPrinter, Int32 pDefault);
        [DllImport("winspool.drv", EntryPoint = "ClosePrinter", CallingConvention = CallingConvention.StdCall)]
            public static extern bool ClosePrinter(Int32 hPrinter);
        [DllImport("winspool.drv", EntryPoint = "FindFirstPrinterChangeNotification", CallingConvention = CallingConvention.StdCall)]
            public static extern IntPtr FindFirstPrinterChangeNotification(
                [InAttribute()] IntPtr hPrinter,
                [InAttribute()] Int32 fwFlags,
                [InAttribute()] Int32 fwOptions,
                [InAttribute(),
                MarshalAs(UnmanagedType.LPStruct)] 
                PRINTER_NOTIFY_OPTIONS pPrinterNotifyOptions);
        [DllImport("winspool.drv", EntryPoint = "FindNextPrinterChangeNotification", CallingConvention = CallingConvention.StdCall)]
            public static extern bool FindNextPrinterChangeNotification(
                [InAttribute()] IntPtr hChangeObject,
                [OutAttribute()] out uint pdwChange,
                [InAttribute(),
                MarshalAs(UnmanagedType.LPStruct)] 
                PRINTER_NOTIFY_OPTIONS pPrinterNotifyOptions,
                [OutAttribute()] out IntPtr lppPrinterNotifyInfo);
        [DllImport("kernel32.dll")]
            static extern UInt32 WaitForMultipleObjects(UInt32 nCount, IntPtr[] lpHandles, bool bWaitAll, UInt32 dwMilliseconds);
        [DllImport("kernel32.dll")]
            static extern IntPtr CreateEvent(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, string lpName);
        [DllImport("kernel32.dll")]
            static extern bool SetEvent(IntPtr hEvent);
        [DllImport("kernel32.dll")]
            internal static extern bool CloseHandle(IntPtr hObject);

        /***** 定数定義 *****/
        private const UInt32 INFINITE = 0xFFFFFFFF;
        private const short DMCOLOR_MONOCHROME = 1;
        private const short DMCOLOR_COLOR = 2;
        private const UInt32 WAIT_OBJECT_0 = 0;
        const int PRINTER_NOTIFY_OPTIONS_REFRESH = 1;

        /***** 変数定義 *****/
        public bool debugMode = false;      //DebugModeでは仮想プリンタ(PDF)のジョブを無視せず、デバッグログを出力する。XPSはDebugModeの設定値に関わらず無視する
        public String debugMessage = "";    //デバッグログのメッセージ

        public Action<PrintInfo> Detected { set; get; }

        PrintInfo printInfo;
        Process[] procList;
        ManagementObjectSearcher procSearcher;  // 必須:System.Management.dll

        private PRINTER_NOTIFY_OPTIONS _notifyOptions = new PRINTER_NOTIFY_OPTIONS();
        private Dictionary<int, string> objJobDict = new Dictionary<int, string>();
        private UInt32 pdwChange = 0;
        private IntPtr pNotifyInfo = IntPtr.Zero;

        private IntPtr _printerHandle = IntPtr.Zero;
        private IntPtr _changeHandle = IntPtr.Zero;
        private IntPtr _stopHandle = (IntPtr)null;
        private IntPtr[] handles = new IntPtr[2];

        private bool runningFlg;
        private Thread getInfoThread = null;

        private PrintTicket ticket = null;      // 必須:ReachFramework.dll
        private PrintQueue printerQueue = null; // 必須:System.Printing.dll
        private int jobId = 0;
        private bool pageMaxFlg = false;
        private bool endFlg = false;
        private Int64 printedJobId = -1;
        private int copyCount = 1;

        private List<String> ignorePrinterList; //印刷情報を無視したいプリンタのプリンタ名
        private bool ignoreFlg = false;

        int jobStatus = 0;
        int preJobId = 0;   //前回印刷時のジョブID

        //Microsoft Edge対応(2017年8月頃？追加。印刷ジョブの出方が他のアプリケーションとだいぶ違うため)
        SuspiciousData suspiciousData = new SuspiciousData();
        Boolean edgeFound = false;

        public void Start()
        {
            if(getInfoThread == null)
            {
                runningFlg = true;
                getInfoThread = new Thread(new ThreadStart(GetPrinterNotificationInfoData));
                getInfoThread.IsBackground = true;

                //無視する仮想プリンタのリスト
                ignorePrinterList = new List<string>();
                //無視判定は部分一致(大文字/小文字は区別しない)
                ignorePrinterList.Add("PDF");
                ignorePrinterList.Add("Microsoft XPS Document Writer");

                getInfoThread.Start();
            }
        }

        private void GetPrinterNotificationInfoData()
        {
            if (OpenPrinter(null, out _printerHandle, 0))
            {
                if (Detected != null)
                {
                    _changeHandle = FindFirstPrinterChangeNotification(_printerHandle, (int)PRINTER_CHANGES. PRINTER_CHANGE_ALL, 0, _notifyOptions);
                    //一部初期化
                    handles[0] = _changeHandle;
                    printInfo = new PrintInfo();
                    printInfo.Title = "";
                    printInfo.PrinterName = "";
                    printInfo.Size = "";
                    printInfo.ExePath = "";

                    while (runningFlg)
                    {
                        Check();    //終了確認
                        //ハンドル初期化
                        try
                        {
                            if(_stopHandle == (IntPtr)null) _stopHandle = CreateEvent(IntPtr.Zero, false, false, "StopEvent");
                            else
                            {
                                //リソース割り当て済みの場合は一度Closeする
                                CloseStopHandle();
                                _stopHandle = CreateEvent(IntPtr.Zero, false, false, "StopEvent");
                            }
                        }
                        catch (Exception) { }
                        finally
                        {
                            handles[1] = _stopHandle;
                        }

                        //印刷ジョブ待ち合わせ
                        if (WaitForMultipleObjects(2, handles, false, INFINITE) == WAIT_OBJECT_0)
                        {
                            try
                            {
                                //印刷ジョブ内容判定
                                if (FindNextPrinterChangeNotification(handles[0], out pdwChange, _notifyOptions, out pNotifyInfo))
                                {
                                    //印刷情報通知を判定し、関係する通知である場合は処理する
                                    if ((pdwChange & PRINTER_CHANGES.PRINTER_CHANGE_SET_JOB) == PRINTER_CHANGES.PRINTER_CHANGE_SET_JOB
                                        || (pdwChange & PRINTER_CHANGES.PRINTER_CHANGE_SET_PRINTER) == PRINTER_CHANGES.PRINTER_CHANGE_SET_PRINTER
                                        || (pdwChange & PRINTER_CHANGES.PRINTER_CHANGE_ADD_JOB) == PRINTER_CHANGES.PRINTER_CHANGE_ADD_JOB
                                        || (pdwChange & PRINTER_CHANGES.PRINTER_CHANGE_WRITE_JOB) == PRINTER_CHANGES.PRINTER_CHANGE_WRITE_JOB)
                                    {
                                        PRINTER_NOTIFY_INFO notifyInfo = (PRINTER_NOTIFY_INFO)Marshal.PtrToStructure(pNotifyInfo, typeof(PRINTER_NOTIFY_INFO));
                                        int pData = (int)pNotifyInfo + Marshal.SizeOf(typeof(PRINTER_NOTIFY_INFO));

                                        //ジョブ情報取得
                                        GetPrinterNotifyInfoData(notifyInfo, pData);

                                        //ログ取得済みジョブか否かを調べる
                                        if (endFlg && (printedJobId != jobId || printedJobId == -1)) endFlg = false;

                                        //戻り値の整備
                                        if(!endFlg)
                                        {
                                            //プリンタの詳細設定情報を取得する
                                            if (printInfo.PrinterName != "" && printerQueue == null)
                                            {
                                                PrintServer srv = new PrintServer();
                                                foreach (var queue in srv.GetPrintQueues())
                                                {
                                                    if (queue.FullName.ToLower().Equals(printInfo.PrinterName.ToLower()))
                                                    {
                                                        queue.Refresh();
                                                        printerQueue = queue;
                                                        ticket = printerQueue.CurrentJobSettings.CurrentPrintTicket;
                                                        break;
                                                    }
                                                }
                                            }

                                            //実行ファイル名取得
                                            if (String.IsNullOrWhiteSpace(printInfo.ExePath)  //ExePathに値が設定されていないこと
                                                && !String.IsNullOrWhiteSpace(printInfo.Title)) //且つTitleに値が設定されていること
                                            {
                                                edgeFound = false;
                                                foreach (var proc in procList)
                                                {
                                                    if (String.IsNullOrWhiteSpace(proc.MainWindowTitle)) continue;
                                                    if (proc.MainWindowTitle.Contains(printInfo.Title))
                                                    {
                                                        printInfo.Title = proc.MainWindowTitle;
                                                        procSearcher = new ManagementObjectSearcher("SELECT ProcessId,ExecutablePath FROM Win32_Process Where ProcessId=" + proc.Id + "And ExecutablePath Is Not Null");
                                                        foreach (ManagementObject item in procSearcher.Get())
                                                        {
                                                            printInfo.ExePath = item.GetPropertyValue("ExecutablePath").ToString();
                                                            //Edge判定(ウィンドウタイトル末尾が"- Microsoft Edge"であり、且つ実行ファイルがApplicationFrameHost.exeであること)
                                                            if (!edgeFound && printInfo.Title.ToLower().EndsWith("- microsoft edge") && Path.GetFileName(printInfo.ExePath).ToLower().Equals("applicationframehost.exe"))
                                                            {
                                                                edgeFound = true;
                                                                suspiciousData.JobId = jobId;
                                                                suspiciousData.JobData.Copy(printInfo);
                                                                if (suspiciousData.PreJobTime == new DateTime() && suspiciousData.PreJobTime != printInfo.AccessTime)
                                                                    suspiciousData.PreJobTime = printInfo.AccessTime;
                                                                setDebugMessage("MicrosoftEdge Suspicious...");
                                                            }
                                                        }
                                                    }
                                                }
                                                if (!String.IsNullOrWhiteSpace(suspiciousData.JobData.Title) && !edgeFound) suspiciousData = new SuspiciousData();
                                            }
                                        }//---戻り値整備 End
                                    }//---印刷情報通知による処理 End
                                }//---if(FindNextPrinterChangeNotification) End
                            }catch (Exception){}
                        }//---if(WaitForMultipleObjects...) End
                    }//---while End
                }
            }
        }

        //印刷情報を取得できていればActionを実行する関数
        private void Check()
        {
            try
            {
                //無視対象であれば無視フラグをONにする
                CheckIgnore();
                //終了条件が揃っていればActionを実行する
                if (((!String.IsNullOrWhiteSpace(printInfo.PrinterName) && pageMaxFlg == true && endFlg == false) || ignoreFlg)
                    && !(String.IsNullOrWhiteSpace(printInfo.Size) && printInfo.PageNum == 0))
                {
                    //印刷日時を取得できていなければ現在の日時を設定する
                    if (printInfo.AccessTime == new DateTime(0))
                    {
                        setDebugMessage("印刷日時を取得できなかったため、代わりに現在時刻を設定します。");
                        printInfo.AccessTime = DateTime.Now;
                        suspiciousData.ForceTimeSet = true;
                    }

                    setDebugMessage("印刷情報取得完了");
                    if (printerQueue != null)
                    {
                        printerQueue.Refresh();
                    }
                    //ここまでの条件をクリアしており、且つページ数を取得できていない場合は印刷禁止処理の残滓と判断する
                    //(他のアプリケーションにより印刷が強制中止されたケースを考慮。そのような場合はページ数が0になる。取得した印刷情報は破棄する。)
                    if (printInfo.PageNum < 1)
                    {
                        suspiciousData.ForceInitExecuted = true;
                        suspiciousData.AbortedJobId = jobId;
                        setDebugMessage("印刷中止");
                        Init(true);
                    }
                    else if (!ignoreFlg)
                    {
                        //Edgeの印刷の可能性が高ければ色情報を修正する
                        if (edgeFound                                   //ウィンドウタイトルに文書名を含むEdgeが起動されている
                            && suspiciousData.ForceTimeSet              //印刷日時取得不可により印刷日時が強制設定されている
                            && suspiciousData.ForceInitExecuted         //一度はページ数取得不可により印刷中止と判定されている
                            && suspiciousData.JobId > jobId             //印刷対象の印刷ジョブのJob IDがロールバックしている
                            && jobId == suspiciousData.AbortedJobId)    //印刷中止と判断された時点のJob IDでの印刷を再度検知した
                        {
                            setDebugMessage("Microsoft Edgeによる印刷と判定しました。");
                            printInfo.Color = suspiciousData.JobData.Color;
                            if (suspiciousData.PreJobTime != new DateTime()) printInfo.AccessTime = suspiciousData.PreJobTime;  //ついでに日時も修正
                            setDebugMessage("印刷情報を修正しました。");
                        }

                        ColorInfoResolver();    //使用したプリンタがカラー印刷をサポートしていない場合はモノクロ印刷として情報を修正する
                        Detected(printInfo);    //アクション実行
                        PrintDetected();        //イベント発行
                        setDebugMessage("印刷情報を送信しました。");
                        CloseStopHandle();  //停止通知ハンドルを解放する
                    }
                    endFlg = true;          //このJobの情報は以降Action実行されないようにする
                    printedJobId = jobId;   //ログ取得済みjobIdを更新

                    //初期化
                    Init(false);
                    suspiciousData.JobData = new PrintInfo();  //JobDataのみ初期化
                }
                //DEBUG しかし今後の対応次第で必要になる可能性があるので残しておく
                //この条件では何か不都合があった気がするが忘れた
                else if (debugMode && ((!String.IsNullOrEmpty(printInfo.PrinterName) && pageMaxFlg == true && endFlg == false) || ignoreFlg))
                {
                    //印刷日時を取得できていなければ現在の日時を設定する
                    if (printInfo.AccessTime == new DateTime(0))
                    {
                        //setDebugMessage("印刷日時を取得できなかったため、代わりに現在時刻を設定します。");
                        printInfo.AccessTime = DateTime.Now;
                    }

                    //setDebugMessage("印刷情報取得完了");
                    if (printerQueue != null) printerQueue.Refresh();

                    if (!ignoreFlg)
                    {
                        ColorInfoResolver();    //使用したプリンタがカラー印刷をサポートしていない場合はモノクロ印刷として情報を修正する
                        setDebugMessage("ignored task...");
                        CloseStopHandle();  //停止通知ハンドルを解放する
                    }
                    endFlg = true;          //このJobの情報は以降Action実行されないようにする
                                            //初期化
                    Init(false);
                    suspiciousData.JobData = new PrintInfo();  //JobDataのみ初期化
                }
                //DEBUG END
            }
            catch (Exception) { }

        }

        private void GetPrinterNotifyInfoData(PRINTER_NOTIFY_INFO info, int pData)
        {
            PRINTER_NOTIFY_INFO_DATA[] data = new PRINTER_NOTIFY_INFO_DATA[info.Count];
            DEVMODE devModeData = new DEVMODE();
            for (uint i = 0; i < info.Count; i++ )
            {
                data[i] = (PRINTER_NOTIFY_INFO_DATA)Marshal.PtrToStructure((IntPtr)pData, typeof(PRINTER_NOTIFY_INFO_DATA));
                pData += Marshal.SizeOf(typeof(PRINTER_NOTIFY_INFO_DATA));
            }
            //WMI
            GetPrintDataWithWMI();  //タイミングによって取得できる情報が変わるので毎回実行する
            for (int i = 0; i < data.Count(); i++)
            {
                if (data[i].Type == (ushort)PRINTERNOTIFICATIONTYPES.JOB_NOTIFY_TYPE)
                {
                    switch (data[i].Field)
                    {
                        case (ushort)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_SUBMITTED:
                            //印刷日時取得
                            SYSTEMTIME time = (SYSTEMTIME)Marshal.PtrToStructure((IntPtr)data[i].NotifyData.Data.pBuf, typeof(SYSTEMTIME));
                            String submittedTime = time.Year.ToString("0000") + time.Month.ToString("00") + time.Day.ToString("00") + (time.Hour + 9).ToString("00") + time.Minute.ToString("00") + time.Second.ToString("00");
                            printInfo.AccessTime = DateTime.ParseExact(submittedTime, "yyyyMMddHHmmss", new System.Globalization.CultureInfo("en-US"));
                            setDebugMessage("JOB_NOTIFY_FIELD_SUBMITTED");
                            break;
                        case (ushort)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_DEVMODE:
                            devModeData = (DEVMODE)Marshal.PtrToStructure((IntPtr)data[i].NotifyData.Data.pBuf, typeof(DEVMODE));

                            //印刷面取得
                            if (devModeData.dmDuplex == (short)Duplex.Simplex) printInfo.Duplex = false;
                            else printInfo.Duplex = true;
                            //用紙サイズ取得
                            if (printInfo.Size == null || printInfo.Size == "")
                                printInfo.Size = String.Format("{0} {1}mm x {2}mm"
                                                                    ,devModeData.dmFormName
                                                                    ,(devModeData.dmPaperLength / 10).ToString()
                                                                    ,(devModeData.dmPaperWidth / 10).ToString());
                            //部数取得
                            if (devModeData.dmCopies > 0) copyCount = devModeData.dmCopies;

                            //色取得
                            switch (devModeData.dmColor)
                            {
                                case DMCOLOR_MONOCHROME:
                                    printInfo.Color = false;
                                    break;
                                case DMCOLOR_COLOR:
                                    printInfo.Color = true;
                                    break;
                                default:
                                    break;
                            }
                            //プリンタ名取得
                            if(printInfo.PrinterName == null || printInfo.PrinterName == "")
                            {
                                printInfo.PrinterName = devModeData.dmDeviceName;
                                SplitPrinterName();
                            }

                            //用紙サイズ解決
                            if (String.IsNullOrWhiteSpace(printInfo.Size) || printInfo.Size.ToLower().Equals("unknown"))
                            {
                                short dmPaperSize = devModeData.dmPaperSize;
                                PaperSizeResolver(dmPaperSize, (IntPtr)data[i].NotifyData.Data.pBuf);
                            }

                            setDebugMessageDevMode(devModeData);
                            break;
                        case (ushort)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_TOTAL_PAGES:
                            //合計ページ数取得
                            int pages = (Int32)data[i].NotifyData.Data.cbBuf;
                            if (pages > printInfo.PageNum) printInfo.PageNum = pages;
                            setDebugMessage("JOB_NOTIFY_FIELD_TOTAL_PAGES -> " + pages + "  operationInfo.PageNum -> " + printInfo.PageNum);
                            break;
                        case (ushort)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_STATUS:
                            if(!pageMaxFlg)
                            {
                                jobStatus = (int)data[i].NotifyData.adwData[0];
                                //JOB_STATUS_DELETEDの時点で情報取得完了と見做す
                                if ((jobStatus & (int)JOBSTATUS.JOB_STATUS_DELETED) == (int)JOBSTATUS.JOB_STATUS_DELETED           //DELETEDを含む
                                     || (
                                        (jobStatus & (int)JOBSTATUS.JOB_STATUS_RETAINED) == (int)JOBSTATUS.JOB_STATUS_RETAINED      //あるいはRETAINEDとPRINTINGを含みSPOOLINGを含まない
                                     && (jobStatus & (int)JOBSTATUS.JOB_STATUS_PRINTING) == (int)JOBSTATUS.JOB_STATUS_PRINTING
                                     && (jobStatus & (int)JOBSTATUS.JOB_STATUS_SPOOLING) != (int)JOBSTATUS.JOB_STATUS_SPOOLING
                                        )
                                     )
                                {
                                    //正確な印刷紙枚数を算出
                                    NumOfPageOptimisation();
                                    //現時点で保持している情報をActionに設定するようフラグをONにする
                                    pageMaxFlg = true;
                                }
                                setDebugMessage("JOB_NOTIFY_FIELD_STATUS -> " + getJobStatusStr(jobStatus));
                            }
                            else setDebugMessage("Page MAX!!\r\nJOB_NOTIFY_FIELD_STATUS -> " + getJobStatusStr(jobStatus));
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void GetPrintDataWithWMI()
        {
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PrintJob"))
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    //JobId
                    if (item.GetPropertyValue("JobId") != null)
                    {
                        jobId = Convert.ToInt32(item.GetPropertyValue("JobId"));

                        //未処理のJobIdであれば変数を初期化する
                        //※万が一、前回の印刷の印刷ログを取得できなかった場合、今回のジョブ処理時に前回のログ情報が残っている可能性があるため。
                        if (preJobId != jobId) Init(false);
                        preJobId = jobId;
                    }
                    //用紙サイズ
                    try
                    {
                        if (item.GetPropertyValue("PaperSize") != null && String.IsNullOrEmpty(printInfo.Size)) printInfo.Size = item.GetPropertyValue("PaperSize").ToString();
                    } catch (Exception) { }
                    //タイトル(ローカル ダウンレベル ドキュメントであれば無視する)
                    if (item.GetPropertyValue("Document") != null && String.IsNullOrEmpty(printInfo.Title)
                        && String.Compare(item.GetPropertyValue("Document").ToString(), "ローカル ダウンレベル ドキュメント", true) != 0)   // English Environment: "local downlevel document"
                    {
                        printInfo.Title = item.GetPropertyValue("Document").ToString();
                    }
                    //プリンタ名
                    if (item.GetPropertyValue("Name") != null && String.IsNullOrEmpty(printInfo.PrinterName))
                    {
                        printInfo.PrinterName = item.GetPropertyValue("Name").ToString();
                        SplitPrinterName();
                    }
                    //色
                    try
                    {
                        if (item.GetPropertyValue("Color") != null)
                        {
                            if ((string)item.GetPropertyValue("Color") == "Color") printInfo.Color = true;
                            else printInfo.Color = false;
                        }
                    }
                    catch (Exception) { }
                    //透かし,印刷面,実行ファイル名はここでは取得できない。印刷日時はすでに取得済み
                    //DEVMODEで取得できないこともあるものはWMIでも取得を試みる
                }
                procList = Process.GetProcesses();
            }

        }

        private void NumOfPageOptimisation()
        {
            double perSheet = 1;
            double side = 1;

            //用紙1面あたりの印刷ページ数取得
            if (ticket != null && ticket.PagesPerSheet != null)
            {
                perSheet = Convert.ToInt32(ticket.PagesPerSheet);
            }
            /* <実際の印刷枚数の算出法>
                * [1部あたりの印刷枚数] ＝ [ドキュメントのページ数] ÷ [用紙1面あたりの印刷ページ数]
                * (両面印刷の場合はさらに÷2かつ少数切り上げ)
                * 実際の印刷枚数の算出法 ＝ [1部あたりの印刷枚数] × [印刷部数]
            */
            printInfo.PageNum = (int)Math.Ceiling(printInfo.PageNum / perSheet / side) * copyCount;
        }

        //取得したジョブのプリンタ名が無視対象か否かを調べる
        private void CheckIgnore()
        {
            String operationPrinter = "";

            //プリンタ名を取得済みであり、DebugModeではなく、且つ無視リストがある場合に判定する
            if (!String.IsNullOrEmpty(printInfo.PrinterName)
                && !debugMode && ignorePrinterList != null && ignorePrinterList.Count > 0)
            {
                operationPrinter = printInfo.PrinterName.ToLower();
                foreach (var printerName in ignorePrinterList)
                {
                    if(operationPrinter.Contains(printerName.ToLower()))
                    {
                        ignoreFlg = true;
                        break;
                    }
                }
            }
        }

        //ログ取得用変数をすべて初期化する
        private void Init(Boolean forceFlg)
        {
            if(forceFlg || (preJobId != 0 && preJobId < jobId))
            {
                setDebugMessage("初期化します。初期化前(preJobId=" + preJobId + ")");
                //初期化
                printInfo = new PrintInfo();
                printInfo.Title = "";
                printInfo.PrinterName = "";
                printInfo.Size = "";
                printInfo.ExePath = "";
                printerQueue = null;
                ticket = null;
                ignoreFlg = false;
                pageMaxFlg = false;
                setDebugMessage("初期化しました。初期化後");
            }
        }

        public void Stop()
		{
            if(getInfoThread != null)
            {
                try
                {
                    runningFlg = false;
                    SetEvent(_stopHandle);
                    getInfoThread.Join();       //スレッド終了待ち合わせ
                    getInfoThread = null;
                    CloseAllHandles();          //ハンドル解放
                }
                catch (Exception) { }
            }
        }

        //デストラクタ
        ~PrintMonitor()
        {
            CloseAllHandles();
        }

        //すべてのハンドルを閉じる関数
        private void CloseAllHandles()
        {
            try
            {
                CloseChangeHandle();
                CloseStopHandle();
            }
            catch (Exception) { }
        }

        //印刷通知のハンドルを閉じる関数
        private void CloseChangeHandle()
        {
            try
            {
                if (_changeHandle != null && _changeHandle != (IntPtr)null && _changeHandle != IntPtr.Zero && !CloseHandle(_changeHandle))
                {
                    throw new Exception(nameof(CloseHandle));
                }else
                {
                    InitChangeHandle();
                }
            }
            catch (Exception) { }
        }

        //停止通知のハンドルを閉じる関数
        private void CloseStopHandle()
        {
            try
            {
                if (_stopHandle != null && _stopHandle != (IntPtr)null && _stopHandle != IntPtr.Zero && !CloseHandle(_stopHandle))
                {
                    throw new Exception(nameof(CloseHandle));
                }else
                {
                    InitStopHandle();
                }
            }
            catch (Exception) { }
        }

        //印刷通知のハンドルを初期化する関数
        private void InitChangeHandle()
        {
            //Closeしてから実行されることが前提
            _changeHandle = (IntPtr)null;
        }

        //停止通知のハンドルを初期化する関数
        private void InitStopHandle()
        {
            //Closeしてから実行されることが前提
            _stopHandle = (IntPtr)null;
        }

        //デバッグメッセージ
        private void setDebugMessage(String str)
        {
            if(debugMode)
            {
                //引数のメッセージを設定
                debugMessage += DateTime.Now + "*******************************\r\n" + str + "\r\n";
                //この時点での各変数値を設定
                if(ignoreFlg)
                {
                    debugMessage += "ignoreFlg=true\r\n";
                }
                debugMessage += "      JobId=" + jobId + "\r\n";
                debugMessage += "  JobStatus=" + getJobStatusStr(jobStatus) + "\r\n";

                if (printInfo != null)
                {
                    debugMessage += " AccessTime=" + printInfo.AccessTime + "\r\n";
                    debugMessage += "      Title=" + printInfo.Title + "\r\n";
                    debugMessage += "PrinterName=" + printInfo.PrinterName + "\r\n";
                    debugMessage += "       Size=" + printInfo.Size + "\r\n";
                    debugMessage += "    ExePath=" + printInfo.ExePath + "\r\n";
                    debugMessage += "    PageNum=" + printInfo.PageNum + "\r\n";
                    debugMessage += "      Color=" + printInfo.Color + "\r\n";
                    debugMessage += "     Duplex=" + printInfo.Duplex + "\r\n";
                }
            }
        }
        //DEVMODE用デバッグメッセージ
        private void setDebugMessageDevMode(DEVMODE data)
        {
            if(debugMode)
            {
                debugMessage += "------JOB_NOTIFY_FIELD_DEVMODE------\r\n";
                debugMessage += data.ToString() + "\r\n";
            }
        }
        //JOBSTATUSの判別
        private String getJobStatusStr(int status)
        {
            String statusStr = "";

            if ((status & (int)JOBSTATUS.JOB_STATUS_PAUSED) == (int)JOBSTATUS.JOB_STATUS_PAUSED)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_PAUSED";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_BLOCKED_DEVQ) == (int)JOBSTATUS.JOB_STATUS_BLOCKED_DEVQ)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_BLOCKED_DEVQ";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_COMPLETE) == (int)JOBSTATUS.JOB_STATUS_COMPLETE)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_COMPLETE";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_DELETED) == (int)JOBSTATUS.JOB_STATUS_DELETED)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_DELETED";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_DELETING) == (int)JOBSTATUS.JOB_STATUS_DELETING)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_DELETING";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_OFFLINE) == (int)JOBSTATUS.JOB_STATUS_OFFLINE)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_OFFLINE";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_ERROR) == (int)JOBSTATUS.JOB_STATUS_ERROR)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_ERROR";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_PAPEROUT) == (int)JOBSTATUS.JOB_STATUS_PAPEROUT)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_PAPEROUT";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_PRINTED) == (int)JOBSTATUS.JOB_STATUS_PRINTED)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_PRINTED";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_PRINTING) == (int)JOBSTATUS.JOB_STATUS_PRINTING)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_PRINTING";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_RENDERING_LOCALLY) == (int)JOBSTATUS.JOB_STATUS_RENDERING_LOCALLY)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_RENDERING_LOCALLY";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_RESTART) == (int)JOBSTATUS.JOB_STATUS_RESTART)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_RESTART";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_RETAINED) == (int)JOBSTATUS.JOB_STATUS_RETAINED)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_RETAINED";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_SPOOLING) == (int)JOBSTATUS.JOB_STATUS_SPOOLING)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_SPOOLING";
            }

            if ((status & (int)JOBSTATUS.JOB_STATUS_USER_INTERVENTION) == (int)JOBSTATUS.JOB_STATUS_USER_INTERVENTION)
            {
                if (statusStr != "")
                {
                    statusStr += " & ";
                }
                statusStr += "JOB_STATUS_USER_INTERVENTION";
            }

            if(String.IsNullOrEmpty(statusStr))
            {
                statusStr = "0x" + status.ToString("X");
            }
            return statusStr;
        }

        //用紙サイズを解決する関数
        private void PaperSizeResolver(short paperSize, IntPtr pDM)
        {
            if (!String.IsNullOrWhiteSpace(printInfo.PrinterName))
            {
                try
                {
                    String result = "";

                    //プリンタがサポートする用紙サイズの名称/ID/サイズの一覧を取得する
                    short[] types = GetSupportedPaperSizeType();
                    String[] names = GetSupportedPaperSizeName(pDM);
                    int[] sizes = GetSupportedPaperSize();

                    //DevModeの用紙サイズIDに対応する用紙サイズの名称とサイズを特定する
                    if (types != null && names != null && sizes != null)
                    {
                        //用紙サイズIDの一覧から、DevModeの用紙サイズIDと一致する要素の添字を探索する
                        for (int index = 0; index < types.Length; index++)
                        {
                            if(types[index] == paperSize)
                            {
                                //用紙サイズ名の一覧から、指定した添字の要素を用紙名として取得する
                                result = names[index];
                                //用紙サイズの一覧から、指定した添字に対応する要素を用紙サイズとして取得する
                                int width = sizes[index * 2] / 10;          //0.1mm単位の値をmm単位に変換する(小数点以下切り捨て)
                                int height = sizes[(index * 2) + 1] / 10;   //0.1mm単位の値をmm単位に変換する(小数点以下切り捨て)
                                result += " " + width + " x " + height + " mm";

                                printInfo.Size = result;
                            }
                        }
                    }
                }
                catch (Exception){}
            }
        }

        //用紙サイズ名の一覧を取得する関数
        private String[] GetSupportedPaperSizeName(IntPtr pDM)
        {
            int DC_PAPERNAMES = 16;
            String[] result = null;
            IntPtr strBuf = IntPtr.Zero;
            try
            {
                long lngBinsCount = DeviceCapabilities(printInfo.PrinterName, "", DC_PAPERNAMES, IntPtr.Zero, IntPtr.Zero);
                long BufSize = (lngBinsCount * 64); //少し多めに
                strBuf = Marshal.AllocHGlobal((int)BufSize); //バッファを確保
                if(strBuf != null && strBuf != (IntPtr)null)
                {
                    DeviceCapabilities(printInfo.PrinterName, "", DC_PAPERNAMES, strBuf, pDM);  //実行
                    if (lngBinsCount > 0)
                    {
                        result = new string[lngBinsCount];
                        string[] names = new string[lngBinsCount];
                        for (int i = 0; i < lngBinsCount; i++)
                        {
                            names[i] = Marshal.PtrToStringAnsi((IntPtr)((i * 64) + (int)strBuf), 64).Split('\0')[0];    //最初のNULL以前の文字列のみ取得
                        }
                        names.CopyTo(result, 0);
                    }
                }
            }
            catch (Exception) {}
            finally
            {
                if (strBuf != null && strBuf != (IntPtr)null && strBuf != IntPtr.Zero)
                {
                    try
                    {
                        Marshal.FreeHGlobal(strBuf);
                    }
                    catch (Exception) { }
                }
            }
            return result;
        }

        //用紙サイズIDの一覧を取得する関数
        private short[] GetSupportedPaperSizeType()
        {
            int DC_PAPERS = 2;
            short[] result = null;
            IntPtr strBuf = IntPtr.Zero;
            try
            {
                long lngBinsCount = DeviceCapabilities(printInfo.PrinterName, "", DC_PAPERS, (IntPtr)null, IntPtr.Zero);
                long BufSize = (lngBinsCount * 2);
                strBuf = Marshal.AllocHGlobal((int)BufSize); //バッファを確保
                if (strBuf != null && strBuf != (IntPtr)null)
                {
                    int res = DeviceCapabilities(printInfo.PrinterName, "", DC_PAPERS, strBuf, IntPtr.Zero);  //実行
                    if (lngBinsCount > 0 && res != -1)
                    {
                        result = new short[lngBinsCount];
                        short[] types = new short[lngBinsCount];
                        for (int i = 0; i < lngBinsCount; i++)
                        {
                            types[i] = Marshal.ReadInt16(strBuf, i * 2);
                        }
                        types.CopyTo(result, 0);
                    }
                }
            }
            catch (Exception) { }
            finally
            {
                if (strBuf != null && strBuf != (IntPtr)null && strBuf != IntPtr.Zero)
                {
                    try
                    {
                        Marshal.FreeHGlobal(strBuf);
                    }
                    catch (Exception) { }
                }
            }
            return result;
        }

        //用紙サイズの一覧を取得する関数
        private int[] GetSupportedPaperSize()
        {
            int DC_PAPERSIZE = 3;
            int[] result = null;
            IntPtr strBuf = IntPtr.Zero;
            try
            {
                long lngBinsCount = DeviceCapabilities(printInfo.PrinterName, "", DC_PAPERSIZE, (IntPtr)null, IntPtr.Zero) * 2; //Widthとheightが必要なので[用紙サイズ数 * 2]
                long BufSize = (lngBinsCount * 8);
                strBuf = Marshal.AllocHGlobal((int)BufSize); //バッファを確保
                if (strBuf != null && strBuf != (IntPtr)null)
                {
                    int res = DeviceCapabilities(printInfo.PrinterName, "", DC_PAPERSIZE, strBuf, IntPtr.Zero);  //実行
                    if (lngBinsCount > 0 && res != -1)
                    {
                        result = new int[lngBinsCount];
                        int[] sizes = new int[lngBinsCount];
                        for (int i = 0; i < lngBinsCount; i++)
                        {
                            sizes[i] = Marshal.ReadInt32(strBuf, i * 4);
                        }
                        sizes.CopyTo(result, 0);
                    }
                }
            }
            catch (Exception) { }
            finally
            {
                if (strBuf != null && strBuf != (IntPtr)null && strBuf != IntPtr.Zero)
                {
                    try
                    {
                        Marshal.FreeHGlobal(strBuf);
                    }
                    catch (Exception) { }
                }
            }
            return result;
        }

        //プリンタ名にJobIDが含まれる場合にJobIDを除去する関数
        private void SplitPrinterName()
        {
            if(!String.IsNullOrWhiteSpace(printInfo.PrinterName) && printInfo.PrinterName.Contains(","))
            {
                printInfo.PrinterName = printInfo.PrinterName.Split(',')[0];
            }
        }

        //色情報を解決する関数
        private void ColorInfoResolver()
        {
            Boolean skipFlg = false;
            try
            {
                //無視リストにある(=仮想プリンタである)プリンタなら印刷ジョブの色情報を優先する
                if (!String.IsNullOrEmpty(printInfo.PrinterName)
                    && ignorePrinterList != null && ignorePrinterList.Count > 0)
                {
                    String operationPrinter = printInfo.PrinterName.ToLower();
                    foreach (var printerName in ignorePrinterList)
                    {
                        if (operationPrinter.Contains(printerName.ToLower()))
                        {
                            skipFlg = true;
                            break;
                        }
                    }
                }

                //物理プリンタであればプリンタドライバの情報を優先する
                if(!skipFlg)
                {
                    //プリンタの色サポート情報を取得する
                    Boolean colorSupport = CheckColorSupport();

                    //カラー印刷の印刷ジョブを検知したがプリンタがカラー印刷をサポートしていない場合、モノクロ印刷としてログを変更する
                    if (printInfo.Color && !colorSupport)
                    {
                        printInfo.Color = false;
                    }
                }
            }
            catch (Exception){ }
        }

        //プリンタがカラー印刷をサポートするか否かを調べる関数(といってもPostScript以外のプリンタドライバでは取得できる情報が不正確だったりする)
        private Boolean CheckColorSupport()
        {
            Boolean result = true;
            try
            {

                PrinterSettings printerSettings = new PrinterSettings();
                printerSettings.PrinterName = printInfo.PrinterName;        //プリンタ名を変更するとオブジェクトの情報が自動的に書き換わる
                result = printerSettings.SupportsColor;                     //カラー印刷に対応していないプリンタであればfalseとなる
            }
            catch (Exception) { }
            return result;
        }

        //イベント定義
        public delegate void PrintMonitorEventHandler(PrintMonitorEventArgs e);
        public event PrintMonitorEventHandler PrintDetectedEvent;
        //イベント発行関数
        public void PrintDetected()
        {
            PrintDetectedEvent(new PrintMonitorEventArgs(PrintMonitorEventArgs.TYPE_PRINT_DETECTED));
        }
    }

    //イベントの情報定義
    public class PrintMonitorEventArgs : EventArgs
    {
        public const int TYPE_PRINT_DETECTED = 1;   //イベント種別: 印刷情報取得完了
        private readonly int _EventType;
        public int EventType { get { return _EventType; } }
        public PrintMonitorEventArgs(int Type)
        {
            _EventType = Type;
        }
    }
    
    //Edgeによる印刷の可能性が高い場合に情報を記録しておくためのクラス
    class SuspiciousData
    {
        public int JobId { set; get; }
        public int AbortedJobId { set; get; }
        public PrintInfo JobData { set; get; }
        public Boolean ForceInitExecuted { set; get; }
        public Boolean ForceTimeSet { set; get; }
        public DateTime PreJobTime { set; get; }

        public SuspiciousData()
        {
            this.JobId = 0;
            this.AbortedJobId = 0;
            this.JobData = new PrintInfo();
            this.ForceInitExecuted = false;
            this.ForceTimeSet = false;
        }
    }

    static class Util
    {
        //ログ出力関数
        public static String osVer = getOsType();
        public static String osBit = Environment.Is64BitOperatingSystem ? "x64" : "x86";
        public static String logDir = AppDomain.CurrentDomain.BaseDirectory + @"\Log\" + osVer.Replace(" ", "") + "_" + osBit + @"\";
        public static String exeDir = AppDomain.CurrentDomain.BaseDirectory + @"\";
        public static bool logging(String logName, String msg)
        {
            bool result = false;
            String logFile = logName + ".txt";
            StreamWriter writer = null;

            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }

            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            try
            {
                writer = new StreamWriter(logDir + logFile, true, sjisEnc);
                writer.WriteLine(msg);
                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }

            return result;
        }

        //OSバージョン取得
        public static String getOsType()
        {
            String str = "";
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem"))
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    if (item.GetPropertyValue("Caption") != null)
                    {
                        str = item.GetPropertyValue("Caption").ToString();
                        Console.WriteLine("Caption：" + str);
                    }
                }
            }
            return str;
        }
    }

    //必要な定義
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct DEVMODE
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string dmDeviceName;

        public short dmSpecVersion;
        public short dmDriverVersion;
        public short dmSize;
        public short dmDriverExtra;
        public int dmFields;

        public short dmOrientation;
        public short dmPaperSize;
        public short dmPaperLength;
        public short dmPaperWidth;
        public short dmScale;
        public short dmCopies;
        public short dmDefaultSource;
        public short dmPrintQuality;
        /*
        public int dmPositionX;
        public int dmPositionY;
        public int dmDisplayOrientation;
        public int dmDisplayFixedOutput;
        */
        public short dmColor;
        public short dmDuplex;
        public short dmYResolution;
        public short dmTTOption;
        public short dmCollate;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string dmFormName;

        public short dmLogPixels;
        public short dmBitsPerPel;
        public int dmPelsWidth;
        public int dmPelsHeight;
        public int dmDisplayFlags;
        public int dmDisplayFrequency;
        public int dmICMMethod;
        public int dmICMIntent;
        public int dmMediaType;
        public int dmDitherType;
        public int dmReserved1;
        public int dmReserved2;
        public int dmPanningWidth;
        public int dmPanningHeight;
        /*
        public override string ToString()
        {
            return string.Format(
                @"dmDeviceName == '{0}',
dmSpecVersion == {1},
dmDriverVersion == {2},
dmSize == {3},
dmDriverExtra == {4},
dmFields == {5},
dmPositionX == {6},
dmPositionY == {7},
dmDisplayOrientation == {8},
dmDisplayFixedOutput == {9},
dmColor == {10},
dmDuplex == {11},
dmYResolution == {12},
dmTTOption == {13},
dmCollate == {14},
dmFormName == {15},
dmLogPixels == {16},
dmBitsPerPel == {17},
dmPelsWidth == {18},
dmPelsHeight == {19},
dmDisplayFlags == {20},
dmDisplayFrequency == {21},
dmICMMethod == {22},
dmICMIntent == {23},
dmMediaType == {24},
dmPanningWidth == {25},
dmPanningHeight == {26}",
                dmDeviceName,
                dmSpecVersion,
                dmDriverVersion,
                dmSize,
                dmDriverExtra,
                dmFields,

                dmPositionX,
                dmPositionY,
                dmDisplayOrientation,
                dmDisplayFixedOutput,

                dmColor,
                dmDuplex,
                dmYResolution,
                dmTTOption,
                dmCollate,
                dmFormName,
                dmLogPixels,
                dmBitsPerPel,
                dmPelsWidth,
                dmPelsHeight,
                dmDisplayFlags,
                dmDisplayFrequency,
                dmICMMethod,
                dmICMIntent,
                dmMediaType,
                dmPanningWidth,
                dmPanningHeight);

        }
        */

        public override string ToString()
        {
            return string.Format(
                @"dmDeviceName == '{0}',
dmSpecVersion == {1},
dmDriverVersion == {2},
dmSize == {3},
dmDriverExtra == {4},
dmFields == {5},

dmOrientation == {6},
dmPaperSize == {7},
dmPaperLength == {8},
dmPaperWidth == {9},
dmScale == {10},
dmCopies == {11},
dmDefaultSource == {12},
dmPrintQuality == {13},

dmColor == {14},
dmDuplex == {15},
dmYResolution == {16},
dmTTOption == {17},
dmCollate == {18},
dmFormName == {19},
dmLogPixels == {20},
dmBitsPerPel == {21},
dmPelsWidth == {22},
dmPelsHeight == {23},
dmDisplayFlags == {24},
dmDisplayFrequency == {25},
dmICMMethod == {26},
dmICMIntent == {27},
dmMediaType == {28},
dmPanningWidth == {29},
dmPanningHeight == {30}",
                dmDeviceName,
                dmSpecVersion,
                dmDriverVersion,
                dmSize,
                dmDriverExtra,
                dmFields,

                dmOrientation,
                dmPaperSize,
                dmPaperLength,
                dmPaperWidth,
                dmScale,
                dmCopies,
                dmDefaultSource,
                dmPrintQuality,

                dmColor,
                dmDuplex,
                dmYResolution,
                dmTTOption,
                dmCollate,
                dmFormName,
                dmLogPixels,
                dmBitsPerPel,
                dmPelsWidth,
                dmPelsHeight,
                dmDisplayFlags,
                dmDisplayFrequency,
                dmICMMethod,
                dmICMIntent,
                dmMediaType,
                dmPanningWidth,
                dmPanningHeight);

        }

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SYSTEMTIME
    {
        [MarshalAs(UnmanagedType.U2)]
        public short Year;
        [MarshalAs(UnmanagedType.U2)]
        public short Month;
        [MarshalAs(UnmanagedType.U2)]
        public short DayOfWeek;
        [MarshalAs(UnmanagedType.U2)]
        public short Day;
        [MarshalAs(UnmanagedType.U2)]
        public short Hour;
        [MarshalAs(UnmanagedType.U2)]
        public short Minute;
        [MarshalAs(UnmanagedType.U2)]
        public short Second;
        [MarshalAs(UnmanagedType.U2)]
        public short Milliseconds;

        public SYSTEMTIME(DateTime dt)
        {
            dt = dt.ToUniversalTime();  // SetSystemTime expects the SYSTEMTIME in UTC
            Year = (short)dt.Year;
            Month = (short)dt.Month;
            DayOfWeek = (short)dt.DayOfWeek;
            Day = (short)dt.Day;
            Hour = (short)dt.Hour;
            Minute = (short)dt.Minute;
            Second = (short)dt.Second;
            Milliseconds = (short)dt.Millisecond;
        }

        public DateTime ToDateTime()
        {
            return new DateTime(Year, Month, Day, Hour, Minute, Second, Milliseconds, CultureInfo.CurrentCulture.Calendar, DateTimeKind.Utc).ToLocalTime();
        }
    }

    [Flags]
    public enum JOBSTATUS
    {
        JOB_STATUS_PAUSED = 0x00000001,
        JOB_STATUS_ERROR = 0x00000002,
        JOB_STATUS_DELETING = 0x00000004,
        JOB_STATUS_SPOOLING = 0x00000008,
        JOB_STATUS_PRINTING = 0x00000010,
        JOB_STATUS_OFFLINE = 0x00000020,
        JOB_STATUS_PAPEROUT = 0x00000040,
        JOB_STATUS_PRINTED = 0x00000080,
        JOB_STATUS_DELETED = 0x00000100,
        JOB_STATUS_BLOCKED_DEVQ = 0x00000200,
        JOB_STATUS_USER_INTERVENTION = 0x00000400,
        JOB_STATUS_RESTART = 0x00000800,
        JOB_STATUS_COMPLETE = 0x00001000,
        JOB_STATUS_RETAINED = 0x00002000,
        JOB_STATUS_RENDERING_LOCALLY = 0x00004000,
    }

    public class PRINTER_CHANGES
    {
        public const uint PRINTER_CHANGE_ADD_PRINTER = 1;
        public const uint PRINTER_CHANGE_SET_PRINTER = 2;   //
        public const uint PRINTER_CHANGE_DELETE_PRINTER = 4;
        public const uint PRINTER_CHANGE_FAILED_CONNECTION_PRINTER = 8;
        public const uint PRINTER_CHANGE_PRINTER = 0xFF;
        public const uint PRINTER_CHANGE_ADD_JOB = 0x100;   //
        public const uint PRINTER_CHANGE_SET_JOB = 0x200;   //
        public const uint PRINTER_CHANGE_DELETE_JOB = 0x400;
        public const uint PRINTER_CHANGE_WRITE_JOB = 0x800; //
        public const uint PRINTER_CHANGE_JOB = 0xFF00;
        public const uint PRINTER_CHANGE_ADD_FORM = 0x10000;
        public const uint PRINTER_CHANGE_SET_FORM = 0x20000;
        public const uint PRINTER_CHANGE_DELETE_FORM = 0x40000;
        public const uint PRINTER_CHANGE_FORM = 0x70000;
        public const uint PRINTER_CHANGE_ADD_PORT = 0x100000;
        public const uint PRINTER_CHANGE_CONFIGURE_PORT = 0x200000;
        public const uint PRINTER_CHANGE_DELETE_PORT = 0x400000;
        public const uint PRINTER_CHANGE_PORT = 0x700000;
        public const uint PRINTER_CHANGE_ADD_PRINT_PROCESSOR = 0x1000000;
        public const uint PRINTER_CHANGE_DELETE_PRINT_PROCESSOR = 0x4000000;
        public const uint PRINTER_CHANGE_PRINT_PROCESSOR = 0x7000000;
        public const uint PRINTER_CHANGE_ADD_PRINTER_DRIVER = 0x10000000;
        public const uint PRINTER_CHANGE_SET_PRINTER_DRIVER = 0x20000000;
        public const uint PRINTER_CHANGE_DELETE_PRINTER_DRIVER = 0x40000000;
        public const uint PRINTER_CHANGE_PRINTER_DRIVER = 0x70000000;
        public const uint PRINTER_CHANGE_TIMEOUT = 0x80000000;
        public const uint PRINTER_CHANGE_ALL = 0x7777FFFF;
    }

    public enum PRINTERPRINTERNOTIFICATIONTYPES
    {
        PRINTER_NOTIFY_FIELD_SERVER_NAME = 0,
        PRINTER_NOTIFY_FIELD_PRINTER_NAME = 1,
        PRINTER_NOTIFY_FIELD_SHARE_NAME = 2,
        PRINTER_NOTIFY_FIELD_PORT_NAME = 3,
        PRINTER_NOTIFY_FIELD_DRIVER_NAME = 4,
        PRINTER_NOTIFY_FIELD_COMMENT = 5,
        PRINTER_NOTIFY_FIELD_LOCATION = 6,
        PRINTER_NOTIFY_FIELD_DEVMODE = 7,
        PRINTER_NOTIFY_FIELD_SEPFILE = 8,
        PRINTER_NOTIFY_FIELD_PRINT_PROCESSOR = 9,
        PRINTER_NOTIFY_FIELD_PARAMETERS = 10,
        PRINTER_NOTIFY_FIELD_DATATYPE = 11,
        PRINTER_NOTIFY_FIELD_SECURITY_DESCRIPTOR = 12,
        PRINTER_NOTIFY_FIELD_ATTRIBUTES = 13,
        PRINTER_NOTIFY_FIELD_PRIORITY = 14,
        PRINTER_NOTIFY_FIELD_DEFAULT_PRIORITY = 15,
        PRINTER_NOTIFY_FIELD_START_TIME = 16,
        PRINTER_NOTIFY_FIELD_UNTIL_TIME = 17,
        PRINTER_NOTIFY_FIELD_STATUS = 18,
        PRINTER_NOTIFY_FIELD_STATUS_STRING = 19,
        PRINTER_NOTIFY_FIELD_CJOBS = 20,
        PRINTER_NOTIFY_FIELD_AVERAGE_PPM = 21,
        PRINTER_NOTIFY_FIELD_TOTAL_PAGES = 22,
        PRINTER_NOTIFY_FIELD_PAGES_PRINTED = 23,
        PRINTER_NOTIFY_FIELD_TOTAL_BYTES = 24,
        PRINTER_NOTIFY_FIELD_BYTES_PRINTED = 25,
    }

    public enum PRINTERJOBNOTIFICATIONTYPES
    {
        JOB_NOTIFY_FIELD_PRINTER_NAME = 0,
        JOB_NOTIFY_FIELD_MACHINE_NAME = 1,
        JOB_NOTIFY_FIELD_PORT_NAME = 2,
        JOB_NOTIFY_FIELD_USER_NAME = 3,
        JOB_NOTIFY_FIELD_NOTIFY_NAME = 4,
        JOB_NOTIFY_FIELD_DATATYPE = 5,
        JOB_NOTIFY_FIELD_PRINT_PROCESSOR = 6,
        JOB_NOTIFY_FIELD_PARAMETERS = 7,
        JOB_NOTIFY_FIELD_DRIVER_NAME = 8,
        JOB_NOTIFY_FIELD_DEVMODE = 9,
        JOB_NOTIFY_FIELD_STATUS = 10,
        JOB_NOTIFY_FIELD_STATUS_STRING = 11,
        JOB_NOTIFY_FIELD_SECURITY_DESCRIPTOR = 12,
        JOB_NOTIFY_FIELD_DOCUMENT = 13,
        JOB_NOTIFY_FIELD_PRIORITY = 14,
        JOB_NOTIFY_FIELD_POSITION = 15,
        JOB_NOTIFY_FIELD_SUBMITTED = 16,
        JOB_NOTIFY_FIELD_START_TIME = 17,
        JOB_NOTIFY_FIELD_UNTIL_TIME = 18,
        JOB_NOTIFY_FIELD_TIME = 19,
        JOB_NOTIFY_FIELD_TOTAL_PAGES = 20,
        JOB_NOTIFY_FIELD_PAGES_PRINTED = 21,
        JOB_NOTIFY_FIELD_TOTAL_BYTES = 22,
        JOB_NOTIFY_FIELD_BYTES_PRINTED = 23,
    }

    [StructLayout(LayoutKind.Sequential)]
    public class PRINTER_NOTIFY_OPTIONS
    {
        public int dwVersion = 2;
        public int dwFlags;
        public int Count = 2;
        public IntPtr lpTypes;

        public PRINTER_NOTIFY_OPTIONS()
        {
            int bytesNeeded = (2 + PRINTER_NOTIFY_OPTIONS_TYPE.JOB_FIELDS_COUNT + PRINTER_NOTIFY_OPTIONS_TYPE.PRINTER_FIELDS_COUNT) * 2;
            PRINTER_NOTIFY_OPTIONS_TYPE pJobTypes = new PRINTER_NOTIFY_OPTIONS_TYPE();
            lpTypes = Marshal.AllocHGlobal(bytesNeeded);
            Marshal.StructureToPtr(pJobTypes, lpTypes, true);
        }
    }

    public enum PRINTERNOTIFICATIONTYPES
    {
        PRINTER_NOTIFY_TYPE = 0,
        JOB_NOTIFY_TYPE = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    public class PRINTER_NOTIFY_OPTIONS_TYPE
    {
        public const int JOB_FIELDS_COUNT = 24;
        public const int PRINTER_FIELDS_COUNT = 23;

        public Int16 wJobType;
        public Int16 wJobReserved0;
        public Int32 dwJobReserved1;
        public Int32 dwJobReserved2;
        public Int32 JobFieldCount;
        public IntPtr pJobFields;
        public Int16 wPrinterType;
        public Int16 wPrinterReserved0;
        public Int32 dwPrinterReserved1;
        public Int32 dwPrinterReserved2;
        public Int32 PrinterFieldCount;
        public IntPtr pPrinterFields;

        private void SetupFields()
        {
            if (pJobFields.ToInt32() != 0)
            {
                Marshal.FreeHGlobal(pJobFields);
            }

            if (wJobType == (short)PRINTERNOTIFICATIONTYPES.JOB_NOTIFY_TYPE)
            {
                JobFieldCount = JOB_FIELDS_COUNT;
                pJobFields = Marshal.AllocHGlobal((JOB_FIELDS_COUNT * 2) - 1);

                Marshal.WriteInt16(pJobFields, 0, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_PRINTER_NAME);
                Marshal.WriteInt16(pJobFields, 2, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_MACHINE_NAME);
                Marshal.WriteInt16(pJobFields, 4, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_PORT_NAME);
                Marshal.WriteInt16(pJobFields, 6, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_USER_NAME);
                Marshal.WriteInt16(pJobFields, 8, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_NOTIFY_NAME);
                Marshal.WriteInt16(pJobFields, 10, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_DATATYPE);
                Marshal.WriteInt16(pJobFields, 12, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_PRINT_PROCESSOR);
                Marshal.WriteInt16(pJobFields, 14, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_PARAMETERS);
                Marshal.WriteInt16(pJobFields, 16, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_DRIVER_NAME);
                Marshal.WriteInt16(pJobFields, 18, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_DEVMODE);
                Marshal.WriteInt16(pJobFields, 20, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_STATUS);
                Marshal.WriteInt16(pJobFields, 22, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_STATUS_STRING);
                Marshal.WriteInt16(pJobFields, 24, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_SECURITY_DESCRIPTOR);
                Marshal.WriteInt16(pJobFields, 26, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_DOCUMENT);
                Marshal.WriteInt16(pJobFields, 28, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_PRIORITY);
                Marshal.WriteInt16(pJobFields, 30, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_POSITION);
                Marshal.WriteInt16(pJobFields, 32, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_SUBMITTED);
                Marshal.WriteInt16(pJobFields, 34, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_START_TIME);
                Marshal.WriteInt16(pJobFields, 36, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_UNTIL_TIME);
                Marshal.WriteInt16(pJobFields, 38, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_TIME);
                Marshal.WriteInt16(pJobFields, 40, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_TOTAL_PAGES);
                Marshal.WriteInt16(pJobFields, 42, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_PAGES_PRINTED);
                Marshal.WriteInt16(pJobFields, 44, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_TOTAL_BYTES);
                Marshal.WriteInt16(pJobFields, 46, (short)PRINTERJOBNOTIFICATIONTYPES.JOB_NOTIFY_FIELD_BYTES_PRINTED);
            }

            if (pPrinterFields.ToInt32() != 0)
            {
                Marshal.FreeHGlobal(pPrinterFields);
            }

            if (wPrinterType == (short)PRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_TYPE)
            {
                PrinterFieldCount = PRINTER_FIELDS_COUNT;
                pPrinterFields = Marshal.AllocHGlobal((PRINTER_FIELDS_COUNT - 1) * 2);

                Marshal.WriteInt16(pPrinterFields, 0, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_SERVER_NAME);
                Marshal.WriteInt16(pPrinterFields, 2, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_PRINTER_NAME);
                Marshal.WriteInt16(pPrinterFields, 4, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_SHARE_NAME);
                Marshal.WriteInt16(pPrinterFields, 6, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_PORT_NAME);
                Marshal.WriteInt16(pPrinterFields, 8, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_DRIVER_NAME);
                Marshal.WriteInt16(pPrinterFields, 10, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_COMMENT);
                Marshal.WriteInt16(pPrinterFields, 12, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_LOCATION);
                Marshal.WriteInt16(pPrinterFields, 14, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_SEPFILE);
                Marshal.WriteInt16(pPrinterFields, 16, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_PRINT_PROCESSOR);
                Marshal.WriteInt16(pPrinterFields, 18, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_PARAMETERS);
                Marshal.WriteInt16(pPrinterFields, 20, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_DATATYPE);
                Marshal.WriteInt16(pPrinterFields, 22, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_ATTRIBUTES);
                Marshal.WriteInt16(pPrinterFields, 24, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_PRIORITY);
                Marshal.WriteInt16(pPrinterFields, 26, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_DEFAULT_PRIORITY);
                Marshal.WriteInt16(pPrinterFields, 28, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_START_TIME);
                Marshal.WriteInt16(pPrinterFields, 30, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_UNTIL_TIME);
                Marshal.WriteInt16(pPrinterFields, 32, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_STATUS_STRING);
                Marshal.WriteInt16(pPrinterFields, 34, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_CJOBS);
                Marshal.WriteInt16(pPrinterFields, 36, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_AVERAGE_PPM);
                Marshal.WriteInt16(pPrinterFields, 38, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_TOTAL_PAGES);
                Marshal.WriteInt16(pPrinterFields, 40, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_PAGES_PRINTED);
                Marshal.WriteInt16(pPrinterFields, 42, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_TOTAL_BYTES);
                Marshal.WriteInt16(pPrinterFields, 44, (short)PRINTERPRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_FIELD_BYTES_PRINTED);
            }
        }

        public PRINTER_NOTIFY_OPTIONS_TYPE()
        {
            wJobType = (short)PRINTERNOTIFICATIONTYPES.JOB_NOTIFY_TYPE;
            wPrinterType = (short)PRINTERNOTIFICATIONTYPES.PRINTER_NOTIFY_TYPE;

            SetupFields();
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PRINTER_NOTIFY_INFO
    {
        public uint Version;
        public uint Flags;
        public uint Count;
    }


    [StructLayout(LayoutKind.Sequential)]
    public struct PRINTER_NOTIFY_INFO_DATA_DATA
    {
        public uint cbBuf;
        public IntPtr pBuf;
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct PRINTER_NOTIFY_INFO_DATA_UNION
    {
        [FieldOffset(0)]
        private uint adwData0;
        [FieldOffset(4)]
        private uint adwData1;
        [FieldOffset(0)]
        public PRINTER_NOTIFY_INFO_DATA_DATA Data;
        public uint[] adwData
        {
            get
            {
                return new uint[] { this.adwData0, this.adwData1 };
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PRINTER_NOTIFY_INFO_DATA
    {
        public ushort Type;
        public ushort Field;
        public uint Reserved;
        public uint Id;
        public PRINTER_NOTIFY_INFO_DATA_UNION NotifyData;
    }

}
